import platform
import sys

import json
import traceback
import uuid
import requests
import os

from .model.dynamic_soko_object import DynamicSokoObject

PATH = os.path.dirname(os.path.abspath(__file__))

sys.path.append("../..")

from .utils import utilities

from .utils.logger import setup_logger
from .utils import exception as exc


class Connector:

    def __init__(self):
        self.logger = setup_logger('root')
        self._user = None
        self.server_url = None
        self.header = "bearer "

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.stop()

    def set_server_url(self, url):
        self.server_url = url
        if self.server_url is None:
            self.server_url = os.getenv('SOKO_SERVER_URL', None)

        if self.server_url == None:
            raise TypeError(
                'Required "server_url" not specified. Pass as argument or set '
                'in environment variable SOKO_SERVER_URL.'
            )

        if not self.server_url.endswith('/api'):
            self.server_url = self.server_url + "/api"

        if not self.server_url.endswith("/"):
            self.server_url = self.server_url + "/"
        else:
            self.server_url = self.server_url

    def user(self):
        return self._user

    def login(self, url, credentials):
        if not self.server_url:
            self.set_server_url(url)
        token = self.get_user_token(credentials)
        self.header = "bearer " + token

    def stop(self):
        self._user = None
        self.server_url = None
        self.header = "bearer "

    def get_user_token(self, credentials=None):
        if credentials == None:
            raise exc.AuthenticationError("Unauthorized")


        try:
            r = requests.post("{}{}/{}".format(self.server_url, "UserSecurity", "Login")
                              , json=json.loads(credentials.toJSON()), verify=False
                              )

            if r.status_code == 401:
                raise exc.AuthenticationError("Unauthorized")

            ret = json.loads(r.text).get("loginOutputInfo")
            if not ret:
                raise exc.AuthenticationError("Unauthorized")

            self._user = DynamicSokoObject(ret["user"])
            return ret["userToken"]
        except exc.AuthenticationError as e:
            raise e
        except Exception as e:
            raise e

    def url(self):
        return self.server_url

    def get(self, obj, method, **kwargs):
        """Generic get method"""
        self.logger.info("GET - {}{}/{}".format(self.server_url, obj, method))
        res = self._post(obj, method, kwargs=kwargs)
        return res

    def input(self, obj, method, **kwargs):
        self.logger.info("POST - {}{}/{}".format(self.server_url, obj, method))
        res = self._input(obj, method, kwargs=kwargs)
        return res

    def updateMany(self, obj, method, sendNotifications, changes):
        """Generic update method"""
        self.logger.info("UPDATE - {}{}/{}".format(self.server_url, obj, method))
        res = self._update(obj, method, sendNotifications, kwargs=changes)
        return res

    def update(self, obj, method, **kwargs):
        """Generic update method"""
        self.logger.info("UPDATE - {}{}/{}".format(self.server_url, obj, method))
        res = self._update(obj, method, kwargs=kwargs)
        return res

    def upload(self, file_path, identifier):
        """Generic upload method"""
        self.logger.info("UPLOAD - {}/{}".format(self.server_url, "attachments"))
        res = self._upload(file_path, identifier)
        return res

    def delete(self, obj, method, **kwargs):
        """Generic delete method"""
        self.logger.info("DELETE - {}{}/{}".format(self.server_url, obj, method))
        res = self._delete(obj, method, kwargs=kwargs)
        return res

    def _delete(self, obj, method, **kwargs):

        if not self.server_url:
            raise exc.AuthenticationError()

        if obj == None or method == None:
            raise exc.ApiError

        _autentification = {"Authorization": self.header, "client-os": platform.system()}

        r = requests.post("{}{}/{}".format(self.server_url, obj, method)
                          , json={"deleteInfo": kwargs["kwargs"]}, headers=_autentification, verify=False
                          )

        if r.status_code == 500:
            raise exc.ServerError(message=str(r.text))

        if r.status_code == 401:
            raise exc.AuthenticationError("Unauthorized")

        try:
            return json.loads(r.text)
        except ValueError:
            raise exc.NotFoundError()

    def _upload2(self, type, file_path, identifier, entity_type, mime_type):

        if not self.server_url:
            raise exc.AuthenticationError()

        try:
            r = requests.post("{}{}/{}".format(self.server_url, type, "UploadFile"),
                              files={'file': (os.path.basename(file_path), open(file_path, 'rb'))},
                              data={
                                  'input':
                                      json.dumps({'fileInfo': {
                                          'fileIdentificator': identifier,
                                          'entityType': entity_type,
                                          'mimeType': mime_type
                                      }})
                              }, verify=False)
        except:
            raise Exception(traceback.format_exc())

        try:
            _output = json.loads(r.text)
            return _output
        except ValueError:
            raise exc.NotFoundError()

    def _upload(self, file_path, identifier):

        if not self.server_url:
            raise exc.AuthenticationError()

        try:
            r = requests.post('{}'.format(self.server_url.replace("/api", "/attachments")),
                              files={'upload': (os.path.basename(file_path), open(file_path, 'rb'))},
                              data={'filename': identifier}, verify=False)
        except:
            raise Exception(traceback.format_exc())

        try:
            _output = json.loads(r.text)
            return _output
        except ValueError:
            raise exc.NotFoundError()

    def _update(self, obj, method, sendNotifications=True, **kwargs):

        if not self.server_url:
            raise exc.AuthenticationError()

        if obj == None or method == None:
            raise exc.ApiError

        _autentification = {"Authorization": self.header, "client-os": platform.system()}

        if isinstance(kwargs["kwargs"], list):
            r = requests.post("{}{}/{}".format(self.server_url, obj, method)
                              , json={"changeInfos": kwargs["kwargs"], "sendNotifications": sendNotifications},
                              headers=_autentification, verify=False
                              )
        else:
            sendNotifications = None
            if "sendNotifications" in kwargs["kwargs"]:
                sendNotifications = kwargs["kwargs"]["sendNotifications"]
                del kwargs["kwargs"]["sendNotifications"]

            r = requests.post("{}{}/{}".format(self.server_url, obj, method)
                              , json={"changeInfo": kwargs["kwargs"], "sendNotifications": sendNotifications},
                              headers=_autentification, verify=False
                              )

        if r.status_code == 401:
            raise exc.AuthenticationError("Unauthorized")

        try:
            _output = json.loads(r.text)
            if not _output["errorInfo"] == None:
                raise exc.Error(
                    "{}\n{}".format(_output["errorInfo"]["errorCodeText"], _output["errorInfo"]["exceptionMessage"]))
            return _output
        except ValueError:
            raise exc.NotFoundError()

    def _post(self, obj, method, **kwargs):
        """post data to server and return result"""

        if not self.server_url:
            raise exc.AuthenticationError()

        if obj == None or method == None:
            raise exc.ApiError

        _autentification = {"Authorization": self.header, "client-os": platform.system()}

        if len(kwargs) > 0:
            payload = {'filter': kwargs["kwargs"]}
        else:
            payload = {}

        r = requests.post("{}{}/{}".format(self.server_url, obj, method)
                          , json=payload, headers=_autentification, verify=False
                          )

        if r.status_code == 401:
            raise exc.AuthenticationError("Unauthorized")

        try:
            _output = json.loads(r.text)
            if not _output["errorInfo"] == None:
                raise exc.Error(
                    "{}\n{}".format(_output["errorInfo"]["errorCodeText"], _output["errorInfo"]["exceptionMessage"]))
            return _output
        except ValueError:
            raise exc.NotFoundError()

    def _input(self, obj, method, **kwargs):
        """post data to server and return result"""

        if not self.server_url:
            raise exc.AuthenticationError()

        if obj == None or method == None:
            raise exc.ApiError

        _autentification = {"Authorization": self.header, "client-os": platform.system()}

        if len(kwargs) > 0:
            payload = {'input': kwargs["kwargs"]}
        else:
            payload = {}

        r = requests.post("{}{}/{}".format(self.server_url, obj, method)
                          , json=payload, headers=_autentification, verify=False
                          )

        if r.status_code == 401:
            raise exc.AuthenticationError("Unauthorized")

        try:
            _output = json.loads(r.text)
            if not _output["errorInfo"] == None:
                raise exc.Error(
                    "{}\n{}".format(_output["errorInfo"]["errorCodeText"], _output["errorInfo"]["exceptionMessage"]))
            return _output
        except ValueError:
            raise exc.NotFoundError()

    def _publish(self, **kwargs):
        publishInfo = self._update("SokoObject", "Publish", kwargs=kwargs)
        return publishInfo["publishInfo"]

    def _publish_external(self, **kwargs):
        publishInfo = self._update("SokoObject", "PublishExternalInput", kwargs=kwargs)
        return publishInfo["publishInfo"]

    def publish_external_input(self, returnCreatedObjects=True, sendNotifications=True, **kwargs):

        soko_object_id = kwargs.get("targetSokoObjectId")
        hidden_soko_object_id = kwargs.get("hiddenSokoObjectId")
        soko_outcome_id = kwargs.get("sokoOutcomeId")
        soko_outcome_type_id = kwargs.get("sokoOutcomeTypeId")
        status_id = kwargs.get("statusId")
        outcome_suffix = kwargs.get("suffix")
        commnet = kwargs.get("comment")
        author_id = kwargs.get("authorId")
        components = kwargs.get("components")
        customProperties = kwargs.get("customProperties")

        if len(components) == 0:
            raise Exception("Components don't exists.")

        if outcome_suffix == None:
            outcome_suffix = ""

        # create components
        chInfos = []
        for component in components:
            try:
                c = dict(
                    id=str(uuid.uuid4()),
                    sokoObjectId=soko_object_id,
                    relativePath=component
                )
                chInfos.append(c)
            except:
                pass

        ch = dict(
            targetSokoObjectId=soko_object_id,
            hiddenSokoObjectId=hidden_soko_object_id,
            sokoOutcomeId=soko_outcome_id,
            sokoOutcomeTypeId=soko_outcome_type_id,
            statusId=status_id,
            suffix=outcome_suffix,
            comment=commnet,
            authorId=author_id,
            components=chInfos,
            customProperties=customProperties,
            sendNotifications=sendNotifications
        )

        _publishInfo = self._publish_external(**ch)

        if returnCreatedObjects:

            _new_components = self.get("SokoObject", "getComponents", sokoObjectid=soko_object_id,
                                       sokoOutcomeVersionId=_publishInfo.get("sokoOutcomeVersionId"))["components"]
            _new_versions = \
                self.get("SokoObject", "getSokoOutcomeVersions", id=_publishInfo.get("sokoOutcomeVersionId"),
                         sokoObjectId=soko_object_id,
                         returnComponents=True, returnNested=True, returnCustomProperties=True)["sokoOutcomeVersions"]

            _new_outcomes = \
                self.get("SokoObject", "getSokoOutcomes", id=_publishInfo.get("sokoOutcomeId"),
                         sokoObjectId=soko_object_id,
                         sokoOutcomeTypeId=soko_outcome_type_id,
                         returnNested=True, returnRelations=True, returnFromHiddenObjects=True)["sokoOutcomes"]
        else:
            _new_components = []
            _new_versions = []
            _new_outcomes = []

        return dict(
            components=_new_components,
            versions=_new_versions,
            outcomes=_new_outcomes,
            componentIds=_publishInfo.get("componentIds", []),
            versionId=_publishInfo.get("sokoOutcomeVersionId"),
            outcomeId=_publishInfo.get("sokoOutcomeId")
        )

    def publish(self, returnCreatedObjects=True, sendNotifications=True, **kwargs):

        soko_object_id = kwargs.get("sokoObjectId")
        soko_outcome_id = kwargs.get("sokoOutcomeId")
        soko_outcome_type_id = kwargs.get("sokoOutcomeTypeId")
        status_id = kwargs.get("statusId")
        outcome_suffix = kwargs.get("suffix")
        commnet = kwargs.get("comment")
        author_id = kwargs.get("authorId")
        components = kwargs.get("components")
        customProperties = kwargs.get("customProperties")

        if len(components) == 0:
            raise Exception("Components don't exists.")

        if outcome_suffix == None:
            outcome_suffix = ""

        # create components
        chInfos = []
        for component in components:
            try:
                c = dict(
                    id=str(uuid.uuid4()),
                    sokoObjectId=soko_object_id,
                    relativePath=component
                )
                chInfos.append(c)
            except:
                pass

        ch = dict(
            sokoObjectId=soko_object_id,
            sokoOutcomeId=soko_outcome_id,
            sokoOutcomeTypeId=soko_outcome_type_id,
            statusId=status_id,
            suffix=outcome_suffix,
            comment=commnet,
            authorId=author_id,
            components=chInfos,
            customProperties=customProperties,
            sendNotifications=sendNotifications
        )

        _publishInfo = self._publish(**ch)

        if returnCreatedObjects:
            _new_components = self.get("SokoObject", "getComponents", sokoObjectid=soko_object_id,
                                       sokoOutcomeVersionId=_publishInfo.get("sokoOutcomeVersionId"))["components"]
            _new_versions = \
                self.get("SokoObject", "getSokoOutcomeVersions", id=_publishInfo.get("sokoOutcomeVersionId"),
                         sokoObjectId=soko_object_id,
                         returnComponents=True, returnNested=True, returnCustomProperties=True)["sokoOutcomeVersions"]

            _new_outcomes = \
                self.get("SokoObject", "getSokoOutcomes", id=_publishInfo.get("sokoOutcomeId"),
                         sokoObjectId=soko_object_id,
                         sokoOutcomeTypeId=soko_outcome_type_id,
                         returnNested=True, returnRelations=True)["sokoOutcomes"]
        else:
            _new_components = []
            _new_versions = []
            _new_outcomes = []

        return dict(
            components=_new_components,
            componentIds=_publishInfo.get("componentIds", []),
            versionId=_publishInfo.get("sokoOutcomeVersionId"),
            outcomeId=_publishInfo.get("sokoOutcomeId"),
            versions=_new_versions,
            outcomes=_new_outcomes,
            publishInfo=_publishInfo
        )
