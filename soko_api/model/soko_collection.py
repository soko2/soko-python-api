
import sys

if sys.version_info[:2] >= (3, 8):
    from collections.abc import MutableSequence
else:
    from collections import MutableSequence


from soko_api.model.dynamic_soko_object import DynamicSokoObject


class SokoCollection(MutableSequence):

    def __init__(self, l, type=DynamicSokoObject):
        super(SokoCollection, self).__init__()
        self._list = []

        for i in l:
            self._list.append(type(i))

    def __add__(self, other):
        return self._list + other._list

    def __repr__(self):
        return repr(self._list)

    def __str__(self):
        return str(self._list)

    def __len__(self):
        return len(self._list)

    def __getitem__(self, index):
        return self._list[index]

    def __setitem__(self, index, value):
        self._list[index] = DynamicSokoObject(value)

    def __delitem__(self, index):
        del self._list[index]

    def insert(self, index, value):
        self._list.insert(index, DynamicSokoObject(value))

    def first(self):
        if len(self._list)==0:
            return None

        return self._list[0]

    def last(self):
        if len(self._list)==0:
            return None

        return self._list[-1]