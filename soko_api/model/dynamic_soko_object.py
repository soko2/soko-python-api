
import json


class DynamicSokoObject(object):
    def __init__(self, d):
        for a, b in d.items():
            if isinstance(b, (list, tuple)):
                setattr(self, a, [DynamicSokoObject(x) if isinstance(x, dict) else x for x in b])
            else:
                setattr(self, a, DynamicSokoObject(b) if isinstance(b, dict) else b)

    def toDict(obj):
        return json.loads(json.dumps(obj, default=lambda o: o.__dict__))

    def __str__(self):
        return str(self.__dict__)

    def __getitem__(self, key):
        if isinstance(key, int):
            return self.__dict__[self.__dict__.keys()[key]]
        else:
            return self.__dict__[key]

    def __setitem__(self, key, value):
        self.__dict__[key]=value

    def get(self, key):
        return self.__dict__.get(key)

    def __len__(self):
        return len(self.__dict__.keys())