from soko_api.model.dynamic_soko_object import DynamicSokoObject
import json


class UploadRecord(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class Setting(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class CloudLink(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class ProjectDefinition(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class SokoObjectTemplate(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class SokoObjectLink(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class PublishInfo(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class CustomAttribute(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class Assignee(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class SokoOutcomeConfiguration(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)
        self.configuration = json.loads(self.configuration)


class NoteCategory(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class User(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class Note(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class Thumbnail(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class Type(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class Component(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class Application(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class SokoOutcome(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class SokoOutcomeType(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class JobInstance(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class Project(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class FileType(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class Priority(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class SokoOutcomeVersion(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class Status(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class FrameIOLabel(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class SokoObjectType(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class AssigneeRole(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)


class SokoObject(DynamicSokoObject):
    def __init__(self, dictionary):
        DynamicSokoObject.__init__(self, dictionary)
