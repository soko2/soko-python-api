# import json
# import os
# import shutil
# import sys
# import time
#
# sys.path.append("../")
#
# # First, import Soko API
# from soko_api import SokoApi
#
# # SokoApi is a Singleton class. You can create instance and save it in to variable, or call constructor each time you access API
# API_KEY = "9f70f4dc-0435-4928-85db-bb7f5d51077a"
# SERVER_URL = "http://localhost:1242/api"
#
# # To login into Soko API with API key and yourt Soko server URL
# api = SokoApi()
# api.set_server_url(SERVER_URL)
# api.login(apiKey=API_KEY)
#
# # Now you can list all projects
# projects = api.getProjects(id="5779a639-9801-4ce6-88cb-7dec5005ac3e")
#
# # To get project objects, you can query 'getSokoObjects' method. In each methods, you can use filters.
# # Filters can be passed as keyword arguments
# # In this example, we list all objects from certain project
# objects = api.getSokoObjects(projectId=projects[0].id)
#
# # [<soko_api.model.models.SokoObject object at 0x03C6F770>, <soko_api.model.models.SokoObject object at 0x03C6F830>, <soko_api.model.models.SokoObject object at 0x03C6F1D0>, <soko_api.model.models.SokoObject object at 0x03C6F2D0>, <soko_api.model.models.SokoObject object at 0x03C6F8D0>, <soko_api.model.models.SokoObject object at 0x03C6F7D0>]
#
# # Creating objects
# # All projects have by default root objects. You can list root object of project like this:
#
# root = api.getSokoObjects(
#     projectId=projects[0].id,
#     returnProjectObjects=True
# )
#
# # To create object, you must provide:
# # projectId - Id of project which own object
# # sokoObjectTypeId - Type of an object. By default, all objects in Soko are generic, SokoObjectType identifies type of an object
# # priorityId - Object priority for artist. Its usefull on 'Task' objects.
# # statusId - Current status of object.
# # name - Name of the object
# # id - UUID identifier of object
#
# object_statuses = api.getStatuses(canBeUsedForSokoObject=True)
# priorities = api.getPriorities()
# task_type = api.getSokoObjectTypes(name="Task")[0]
# folder_type = api.getSokoObjectTypes(name="Folder")[0]
# compositing_type = api.getTypes(name="Compositing")[0]
#
# # create new Folder object, use root object ID as parentId of new Folder:
# folder = api.createSokoObject(
#     id="a7e904dd-aa70-488e-97c3-6da70d0083f8",
#     parentId=root[0].id,
#     projectId=projects[0].id,
#     sokoObjectTypeId=folder_type.id,
#     priorityId=priorities[0].id,
#     statusId=object_statuses[0].id,
#     name="new folder"
# )
#
# # create new Task object, use our Folder object ID as parentId of new Task. For "Taskable" objects, you can also specify TypeId:
# task = api.createSokoObject(
#     id="594be487-ea2e-45b8-9a4c-91f54c963c9e",
#     parentId=folder["response"],
#     projectId=projects[0].id,
#     sokoObjectTypeId=task_type.id,
#     priorityId=priorities[0].id,
#     statusId=object_statuses[0].id,
#     name="new task",
#     typeId=compositing_type.id # Compositing
# )
#
# task_id = "594be487-ea2e-45b8-9a4c-91f54c963c9e"
#
# # Assign User to Task
# # You can assign User to Task. For assign user, you need to provide AssigneeRoleTypeId.
# # In Soko are 3 types of Assignee Role:
# # Assignee - 12b42675-3456-4708-bf07-884775f88cd7
# # Reviewer - dc44f144-e264-4583-b02c-5f2afbaf8a72
# # Manager - 3a9e2653-4de8-4b4e-a210-2ffa48408648
#
# # To list all Assignee Roles, call this method:
# assignee_roles = api.getAssigneeRoles()
#
# # List Users
# users = api.getUsers(id="cb29bcdc-ec2f-447f-9872-8d63eb3fc2f6")
#
# assignment = api.addAssigneeToSokoObject(
#     id="8653f5aa-f857-438c-9ede-86f27da47d40",
#     sokoObjectId=task_id,
#     userId=users[0].id,
#     assigneeRoleId="12b42675-3456-4708-bf07-884775f88cd7"
# )
#
# # Update object
# # To update object, call updateSokoObject method with ID of object you want update and new property:
# updateResponse = api.updateSokoObject(
#     id=task_id,
#     statusId="3e788227-5759-440c-abe3-8a1abbe3f502" # Ready To Start
# )
#
# # Delete object
# # To detele object, call deleteSokoObject method with ID of object you want to delete
# deleteResponse = api.deleteSokoObject(
#     id=task_id
# )
#
# # Publishing new version
#
# # Each Output must have Type. To get type, you can call getOutcomeTypes method
# image_type = api.getOutcomeTypes(name="Image")
#
# # Create Output
# outcome = api.createSokoOutcome(
#     id="b02aea03-548e-43aa-afc0-acf54e88fade",
#     sokoObjectId=task_id,
#     sokoOutcomeTypeId=image_type[0].id,
#     name="my image"
# )
#
# # By default, Soko will generate Output name by your Project Definition settings. With manual publishing via API, you can pass yourt custom Output name or
# # grab one from API:
# outcome_name = api.getSokoOutcomeDefaultName(
#     sokoObjectId=task_id,
#     sokoOutcomeTypeId=image_type[0].id,
#     sokoOutcomeSuffix="additional name"
# )
#
# outcome_id = "b02aea03-548e-43aa-afc0-acf54e88fade"  # outcome["response"]
#
# # List only statuses for outcomes
# outcome_statuses = api.getStatuses(canBeUsedForSokoOutcome=True)
#
# version = api.createSokoOutcomeVersion(
#         id="70ec7e06-f872-42c4-bf87-d241cb9a8e17",
#         sokoOutcomeId=outcome_id,
#         authorId=users[0].id,
#         statusId=outcome_statuses[0].id,
#         comment="new version!"
# )
#
# version_id = "70ec7e06-f872-42c4-bf87-d241cb9a8e17"  # version["response"]
#
# # List file types
# jpeg_file_type = api.getFileTypes(fileExtension=".jpeg")
#
# # Add files to version
# components = api.createComponents([
#     {
#         "id": "2e870ead-6929-4add-903e-524112867897",
#         "sokoOutcomeVersionId": version_id,
#         "relativePath": "path/to/file.jpeg"
#     }
# ])
#
# # To get file name generated from Project Definition settings, call:
# file_name = api.getSokoOutcomeVersionFileName(
#     sokoObjectId=task_id,
#     sokoOutcomeTypeId=image_type[0].id,
#     sokoOutcomeSuffix="additional name",
#     sokoOutcomeName=outcome_name,
#     publisherId=users[0].id,
#     version=1
# )
#
# # To get file path generated from Project Definition settings, call:
# output_paths = api.getSokoOutcomePublishPath(
#     sokoObjectId=task_id,
#     sokoOutcomeTypeId=image_type[0].id,
#     sokoOutcomeName=outcome_name,
#     sokoOutcomeSuffix="additional name",
#     publisherId=users[0].id
# )
#
# # You can get all needed data in one call. If you specify "sokoOutcomeId", next version number is automatically used
# # or you can specify yout custom version number with "versionNumber" parameter:
# publish_info = api.getPublishInfo(
#     sokoObjectId=task_id,
#     sokoOutcomeTypeId=image_type[0].id,
#     sokoOutcomeName=outcome_name,
#     sokoOutcomeId=outcome_id,
#     suffix="additional name",
#     authorId=users[0].id
# )
#
# # For simplification publish process, you can use "publish" method.
# # You can also save some metadata by pass "customProperties" parameter as JSON string
# res = api.publish(
#     sokoObjectId=task_id,
#     sokoOutcomeId=outcome_id,
#     sokoOutcomeTypeId=image_type[0].id,
#     statusId=outcome_statuses[0].id,
#     suffix="additional name",
#     comment="new version!",
#     components=[
#         "path/to/file.jpeg"
#     ],
#     returnCreatedObjects=True
# )
#
# # Complete publish example
# publish_info = api.getPublishInfo(
#     sokoObjectId=task_id,
#     sokoOutcomeTypeId=image_type[0].id,
#     suffix="additional name",
#     authorId=users[0].id
# )
#
# publish_paths = publish_info["publishPaths"]
# file_name = publish_info["fileName"]
#
# source = "C:/Users/blue/Desktop/test2_mp4/skuska/01_01_C1_010_Lgt_merge_v001/test.0600.jpg"
#
# components = []
# for path in publish_paths:
#     destination = '/'.join([path, file_name + os.path.splitext(source)[-1]])
#
#     if not os.path.exists(path):
#         os.makedirs(path)
#
#     shutil.copy2(source, destination)
#     components.append(destination)
#
# metadata = {
#     "key": "value"
# }
#
# publish_result = api.publish(
#     sokoObjectId=task_id,
#     sokoOutcomeId=outcome_id,
#     sokoOutcomeTypeId=image_type[0].id,
#     statusId=outcome_statuses[0].id,
#     suffix="additional name",
#     comment="new version!",
#     components=components,
#     customProperties=json.dumps(metadata),
#     returnCreatedObjects=True
# )