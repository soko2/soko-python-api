import json
import re, os
import hashlib
import traceback


def to_camel(chars):
    word_regex_pattern = re.compile("[^A-Za-z]+")
    words = word_regex_pattern.split(chars)
    return "".join(w.title() for i, w in enumerate(words))


def merge_dicts(*dict_args):
    """
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    """
    result = {}
    for dictionary in dict_args:
        result.update(json.loads(dictionary))
    return result

def is_collection(obj):
    try:
        iter(obj)
        return True
    except TypeError:
        return False

def get_extension(path):
    try:
        return os.path.splitext(path)[-1]
    except:
        raise

def hash_component(path, ext=None, verbose=0):

    BUF_SIZE = 65536

    md5 = hashlib.md5()

    if os.path.isfile(path):
        with open(path, 'rb') as f:
            if verbose == 1:
                print ('Hashing', os.path.basename(path))
            file_buffer = f.read(BUF_SIZE)
            while len(file_buffer) > 0:
                md5.update(file_buffer)
                file_buffer = f.read(BUF_SIZE)
    else:
        directory=os.path.dirname(path)
        try:
            for root, dirs, files in os.walk(directory):
                for names in files:
                    if verbose == 1:
                        print ('Hashing', names)
                    filepath = os.path.join(root, names)
                    if ext:
                        if not filepath.endswith(ext):continue
                    try:
                        f1 = open(filepath, 'rb')
                    except:
                        f1.close()
                        continue

                    while 1:
                        # Read file in as little chunks
                        buf = f1.read(BUF_SIZE)
                        if not buf: break
                        md5.update(hashlib.md5(buf).hexdigest())
                    f1.close()

        except:
            traceback.print_exc()

    return md5.hexdigest()

# import time
# s=time.time()
# file=r"\\storage05\project\asura\asset\prop\chandelier\publish\lookdev\scene\chandelier_lookdev_v001.mb"
# seq=r"\\storage05\project\asura\sequence\052_PRB_0000\052_PRB_0010\render\2d\publish\052_prb_0010_comp\052_prb_0010_comp_v028\052_prb_0010_comp_v028.####.exr"
#
# print hash_component(seq, "exr", 1)
# print time.time()-s