import sys
import traceback


class Error(Exception):

    default_message = 'An unspecified error occurred.'

    def __init__(self, message=None, details=None):
        '''Initialise exception with *message*.

        If *message* is None, the class 'default_message' will be used.

        *details* should be a mapping of extra information that can be used in
        the message and also to provide more context.

        '''
        if message is None:
            message = self.default_message

        self.message = message
        self.details = details
        if self.details is None:
            self.details = {}

        self.traceback = traceback.format_exc()

    def __str__(self):

        keys = {}
        for key, value in self.details.items():
            if isinstance(value, unicode):
                value = value.encode(sys.getfilesystemencoding())
            keys[key] = value

        return str(self.message.format(**keys))

class ApiError(Error):
    default_message = "An API error occured."

class AuthenticationError(Error):
    default_message = 'An authentication error occured.'

class ServerError(Error):
    default_message = 'Server reported error processing request.'

class RequestError(Error):
    default_message = 'An request error occured.'

class NotFoundError(Error):
    default_message = 'Not found.'