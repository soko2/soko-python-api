import json


class Credentials:

    def __init__(self, userName=None, password=None, personalApiKey=None, apiKey=None):
        self.loginInfo = {}

        self.loginInfo = {
            "userName": userName, "password": password, "personalApiKey": personalApiKey, "apiKey": apiKey
        }

    def __len__(self):
        return len(self.__dict__)

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)