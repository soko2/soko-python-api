import json


class DeleteInfo:

    def __init__(self, **kwargs):
        if isinstance(kwargs, DeleteInfo):
            self.deleteInfo = kwargs
        elif isinstance(kwargs, dict) and kwargs.get("kwargs", None) == None:
            self.deleteInfo = kwargs
        elif len(kwargs) > 0:
            self.deleteInfo = kwargs[kwargs.keys()[0]]
        else:
            self.deleteInfo = {}

    def __str__(self):
        return str(self.deleteInfo)

    def __setitem__(self, key, value):
        self.deleteInfo[key]=value

    def add_attribute(self, key, value):
        self.deleteInfo[key] = value

    def __len__(self):
        return len(self.__dict__)

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)
