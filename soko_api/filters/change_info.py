import json

class Update:

    def __init__(self, path, value, op="replace"):
        self.path=path
        self.value=value
        self.op=op

        if not self.path.startswith("/"):
            self.path="/"+self.path

    def toDict(self):
        return self.__dict__

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                   sort_keys=True, indent=4)

class JsonPathChangeInfo:

    def __init__(self, id, changeInfoName, updates=[]):
        self.changeInfoId=id
        self.changeInfoName=changeInfoName
        self.updates=updates

    def __len__(self):
        return len(self.__dict__)

    def addUpdate(self, update):
        self.updates.append(update)

    def toJSON(self):
        updateInfo={}
        updateInfo[self.changeInfoName]=[]

        for u in self.updates:
            updateInfo[self.changeInfoName].append(u.toDict())

        updateInfo["changeInfoId"]=self.changeInfoId

        return json.dumps(updateInfo)

class ChangeInfo:

    def __init__(self, **kwargs):
        if isinstance(kwargs, ChangeInfo):
            self.changeInfo = kwargs
        elif isinstance(kwargs, dict) and kwargs.get("kwargs", None) == None:
            self.changeInfo = kwargs
        elif len(kwargs) > 0:
            self.changeInfo = kwargs[kwargs.keys()[0]]
        else:
            self.changeInfo = {}

    def __str__(self):
        return str(self.changeInfo)

    def __setitem__(self, key, value):
        self.changeInfo[key]=value

    def add_attribute(self, key, value):
        self.changeInfo[key] = value

    def __len__(self):
        return len(self.__dict__)

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

class ChangeInfos:

    def __init__(self, infos=[]):
        self.changeInfos = [i.changeInfo for i in infos]

    def __str__(self):
        return str(self.changeInfos)

    def __len__(self):
        return len(self.__dict__)

    def append(self, val):
        self.changeInfos.append(val.changeInfo)

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)