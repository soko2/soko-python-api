import os
import json
import platform

from .connector import Connector
from .filters.credentials import *
from .model.dynamic_soko_object import DynamicSokoObject
from .model.models import *
from .model.soko_collection import SokoCollection
from .utils.singleton import Singleton

from .constants import *


class SokoApi(Singleton):

    def __init__(self):
        self.connector = Connector()

    def set_server_url(self, url):
        """Set server url

        Args:
            url (str): Server URL
        """

        self.connector.set_server_url(url)

    def server_url(self):
        """Get server url
        """

        return self.connector.server_url

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.logout()

    def __del__(self):
        self.logout()

    def user(self):
        """Get logged user
        """

        return self.connector.user()

    def login(self, userName=None, password=None, personalApiKey=None, apiKey=None):
        """Login

        Args:
           userName (str): UserName.
           password (str): Password.
           personalApiKey (str): User API Key.
           apiKey (str): API Key.

        """

        li = Credentials(userName, password, personalApiKey, apiKey)

        self.connector.login(self.connector.server_url, credentials=li)

        self._userName = userName

    def logout(self):
        pass
        # self.connector.stop()

    def getPublishInfo(self, **kwargs):
        """Get data for publish

        Keyword Arguments:
           returnNested (bool): Return nested properties.
           returnRelations (bool): Return related properties.
           returnRelatedToUserIds (bool): Return list of User IDs related to object.
           userOS (str): Client operating system.
           sokoObjectId (str): SokoObject ID.
           sokoOutcomeId (str): Output ID.
           sokoOutcomeVersionId (str): Version ID.
           versionNumber (str): Custom Version number.
           sokoOutcomeTypeId (str): Output Type ID.
           authorId (str): Author ID.
           suffix (str): Additional Output name.
           isExternal (bool): Return paths for ExternalInput.
           isForWeb (bool): Return web paths.
        """

        res = self.connector.get("SokoObject", "getPublishInfo", **kwargs)
        return res["publishInfo"]

    def getProjectViews(self, **kwargs):
        """List ProjectViews

            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object identifier.
            ownerId (str):
            viewGroupId (str):
            sokoObjectTypeId (str): Object Type ID
            projectId (str): Project ID
            sokoObjectId (str): SokoObject ID
            filter (str):
            name (str): ProjectView name
            isPublic (bool): If ProjectView is public or private

        """
        res = self.connector.get("UserManagement", "getProjectViews", **kwargs)
        return SokoCollection(res["projectViews"], ProjectView)

    def updateProjectView(self, **kwargs):
        """Updates ProjectView

        Keyword Arguments:

        """

        res = self.connector.update("UserManagement", "updateProjectView", **kwargs)
        return res

    def getProjectCustomAttributes(self, **kwargs):
        """List Project CustomAttributes

            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            projectId (str): Project ID.

        KeyValue:
            **projectId** (str) - Project ID.
        """
        res = self.connector.get("Project", "getCustomAttributes", **kwargs)
        return SokoCollection(res["customAttributes"], CustomAttribute)

    def getCustomAttributes(self, **kwargs):
        """List CustomAttributes

        Keyword Arguments:
           returnNested (bool): Return nested properties.
           returnRelations (bool): Return related properties.
           returnRelatedToUserIds (bool): Return list of User IDs related to object.
           userOS (str): Client operating system.
           id (str): Object identifier.
           codeName (str): CodeName.
           sokoObjectId (str): SokoObject ID.
           sokoObjectIds (list): List of SokoObject IDs.
           projectId (str): Project ID.
           returnRecursive (bool): Return all child attributes.
           returnProjectAttributes (bool): Return project attributes.
           customAttributeDefinitionIds (list): List of CustomAtributeDefinition IDs.
           sokoObjectCustomAttributeDefinitionPairs (list of KeyValue): List of KeyValue pairs.

        KeyValue:
            **sokoObjectId** (str) - SokoObject ID.

            **customAttributeDefinitionId** (str) - CustomAttributeDefinition ID.

        """

        res = self.connector.get("SokoObject", "getCustomAttributes", **kwargs)
        return SokoCollection(res["customAttributes"], CustomAttribute)

    def projectGetCustomAttributes(self, **kwargs):
        res = self.connector.get("Project", "getCustomAttributes", **kwargs)
        return SokoCollection(res["customAttributes"], CustomAttribute)

    def createUpdateCustomAttribute(self, **kwargs):
        """Create or Update CustomAttribute

        Keyword Arguments:
           returnNested (bool): Return nested properties.
           returnRelations (bool): Return related properties.
           returnRelatedToUserIds (bool): Return list of User IDs related to object.
           userOS (str): Client operating system.
           id (str): Object identifier.
           customAttributeDefinitionId (str): CustomAttributeDefinition ID.
           value (str): Attribute value.
           sokoObjectId (str): SokoObject ID.
           projectId (str): Project ID.
        """

        res = self.connector.update("SokoObject", "createUpdateCustomAttribute", **kwargs)
        return res

    def createUpdateCustomAttributeBulk(self, changes, sendNotifications=True):
        """Create or Update CustomAttributes

        Args:
            changes (list of Change):  Changes.
            sendNotifications (bool): Send server notification about change

        Change:
            **returnNested** (bool) - Return nested properties.

            **returnRelations** (bool) - Return related properties.

            **returnRelatedToUserIds** (bool) - Return list of User IDs related to object.

            **userOS** (str) - Client operating system.

            **id** (str) - Object identifier.

            **customAttributeDefinitionId** (str) - CustomAttributeDefinition ID.

            **value** (str) - Attribute value.

            **sokoObjectId** (str) - SokoObject ID.

            **projectId** (str) - Project ID.

        """

        res = self.connector.updateMany("SokoObject", "createUpdateCustomAttributeBulk", sendNotifications, changes)
        return res

    def shareComponents(self, changes, sendNotifications=True):
        res = self.connector.updateMany("SokoSync", "shareComponents", sendNotifications, changes)
        return res

    def getSokoObjectContentPath(self, **kwargs):
        """Get drive path for Object

        Keyword Arguments:
           returnNested (bool): Return nested properties.
           returnRelations (bool): Return related properties.
           returnRelatedToUserIds (bool): Return list of User IDs related to object.
           userOS (str): Client operating system.
           sokoObjectId (str): SokoObject ID.
           userId (str): Return path for concrete User.
           content (list): Path type (wip, plate, temp_3d_render, temp_2d_render).
           returnForExplore (bool): Return path for open in system explorer.
           returnUnparsedTemplatesOnly (bool): Return unparsed path templates.
        """

        res = self.connector.get("SokoObject", "getSokoObjectContentPath", **kwargs)
        return res["response"]

    def getSokoObjectContentPathBulk(self, **kwargs):
        """Get drive path for Object

        Keyword Arguments:
           returnNested (bool): Return nested properties.
           returnRelations (bool): Return related properties.
           returnRelatedToUserIds (bool): Return list of User IDs related to object.
           userOS (str): Client operating system.
           sokoObjectId (str): SokoObject ID.
           userId (str): Return path for concrete User.
           content (list): Path type (wip, plate, temp_3d_render, temp_2d_render).
           returnForExplore (bool): Return path for open in system explorer.
           returnUnparsedTemplatesOnly (bool): Return unparsed path templates.
        """

        res = self.connector.get("SokoObject", "getSokoObjectContentPathBulk", **kwargs)
        return res["response"]

    def getSokoObjectDirectoryStructure(self, **kwargs):
        """Get drive paths for Object for create on drive

        Keyword Arguments:
           returnNested (bool): Return nested properties.
           returnRelations (bool): Return related properties.
           returnRelatedToUserIds (bool): Return list of User IDs related to object.
           userOS (str): Client operating system.
           sokoObjectId (str): SokoObject ID.
        """

        res = self.connector.get("SokoObject", "getSokoObjectDirectoryStructure", **kwargs)
        return res["response"]

    def getSokoObjectDirectoryStructureBulk(self, **kwargs):
        """Get drive paths for Objects for create on drive

        Keyword Arguments:
           returnNested (bool): Return nested properties.
           returnRelations (bool): Return related properties.
           returnRelatedToUserIds (bool): Return list of User IDs related to object.
           userOS (str): Client operating system.
           sokoObjectIds (list): List of SokoObject IDs.
        """

        res = self.connector.get("SokoObject", "getSokoObjectDirectoryStructureBulk", **kwargs)
        return res["response"]

    def getSokoOutcomePathBulk(self, **kwargs):
        """Get drive paths for Objects for create on drive

        Keyword Arguments:
           returnNested (bool): Return nested properties.
           returnRelations (bool): Return related properties.
           returnRelatedToUserIds (bool): Return list of User IDs related to object.
           userOS (str): Client operating system.
           sokoOutcomeIds (list): List of SokoOutcome IDs.
           publisherId (str): Publisher ID.
        """

        res = self.connector.get("SokoObject", "getSokoOutcomePathBulk", **kwargs)
        return res["sokoOutcomePaths"]

    def getSettings(self, **kwargs):
        """List Settings

        Keyword Arguments:
           returnNested (bool): Return nested properties.
           returnRelations (bool): Return related properties.
           returnRelatedToUserIds (bool): Return list of User IDs related to object.
           userOS (str): Client operating system.
           id (str): Object identifier.
           key (list): Key.
           type (int): Type (0 - none, 1 - integration).
           isCore (bool): IsCore.
        """

        res = self.connector.get("UserManagement", "getSettings", **kwargs)
        return SokoCollection(res["settings"], Setting)

    def getSokoObjectLinks(self, **kwargs):
        """List SokoObjectLinks

        Keyword Arguments:
           returnNested (bool): Return nested properties.
           returnRelations (bool): Return related properties.
           returnRelatedToUserIds (bool): Return list of User IDs related to object.
           userOS (str): Client operating system.
           sokoObjectId (str): SokoObject ID.
           sokoObjectIds (list): List of SokoObject IDs.
           projectId (str): Project ID.
        """

        res = self.connector.get("SokoObject", "getSokoObjectLinks", **kwargs)
        return SokoCollection(res["sokoObjectLinks"], SokoObjectLink)

    def getSokoObjectLinksFlat(self, **kwargs):
        """List SokoObjectLinks flat

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            sokoObjectId (str): SokoObject ID.
            sokoObjectIds (list): List of SokoObject IDs.
            projectId (str): Project ID.
            projectIds (list): List of Project IDs.
            inputSokoObjectId(str): InputSokoObject ID.
            outputSokoObjectId(str): OutputSokoObject ID.
        """

        res = self.connector.get("SokoObject", "getSokoObjectLinksFlat", **kwargs)
        return SokoCollection(res["sokoObjectLinks"], SokoObjectLink)

    def getJobInstances(self, **kwargs):
        """List JobInstances

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object identifier.
            ids (list): List of Object identifiers.
            authorId (str): Author ID.
            fromDate (str): Start date range.
            toDate (str): End date range.
            contextId (str): Context ID.
            contextType (str): Context Type (project, soko_object, version).
            returnContextPath (bool): Return Context path if Context is an SokoObject.
        """

        res = self.connector.get("Job", "getJobInstances", **kwargs)
        return SokoCollection(res["jobInstances"], JobInstance)

    def setJobs(self, jobs, sendNotifications=True):
        """Set Jobs

        Args:
            jobs (list of Job): Jobs.
            sendNotifications (bool): Send server notification about change

        Job:
            **id** (str) - Object identifier.

            **name** (str) - Job name.

            **label** (str) - Job label.

            **isForProject** (bool) - If job is for Project.

            **isForSokoObject** (bool) - If job is for Object.

            **isForSokoOutcomeVersion** (bool) - If job is for Version.

            **isForMultipleObjects** (bool) - If job is for multiple objects.

            **askForConfirmation** (bool) - If true, display confirmation dialog before run.

            **queueName** (str) - Queue Name.

            **maxRetries** (str) - Max number of retries.

            **widget** (str) - Job Widget (JSON Form).

            **folder** (str) - Job Group.

            **icon** (str) - Font Awesome Icon.

        """

        res = self.connector.updateMany("Template", "setJobs", sendNotifications, jobs)
        return None

    def addJobs(self, jobs, sendNotifications=True):
        """Add Jobs

        Args:
            jobs (list of Job): Jobs.
            sendNotifications (bool): Send server notification about change

        Job:
            **id** (str) - Object identifier.

            **name** (str) - Job name.

            **label** (str) - Job label.

            **isForProject** (bool) - If job is for Project.

            **isForSokoObject** (bool) - If job is for Object.

            **isForSokoOutcomeVersion** (bool) - If job is for Version.

            **isForMultipleObjects** (bool) - If job is for multiple objects.

            **askForConfirmation** (bool) - If true, display confirmation dialog before run.

            **queueName** (str) - Queue Name.

            **maxRetries** (str) - Max number of retries.

            **widget** (str) - Job Widget (JSON Form).

            **folder** (str) - Job Group.

            **icon** (str) - Font Awesome Icon.

        """

        res = self.connector.updateMany("Template", "addJobs", sendNotifications, jobs)
        return None

    def removeJobs(self, jobs, sendNotifications=True):
        """Remove Jobs

        Args:
            jobs (list of Job): Jobs.
            sendNotifications (bool): Send server notification about change

        Job:
            **id** (str) - Object identifier.

            **name** (str) - Job name.

            **label** (str) - Job label.

            **isForProject** (bool) - If job is for Project.

            **isForSokoObject** (bool) - If job is for Object.

            **isForSokoOutcomeVersion** (bool) - If job is for Version.

            **isForMultipleObjects** (bool) - If job is for multiple objects.

            **askForConfirmation** (bool) - If true, display confirmation dialog before run.

            **queueName** (str) - Queue Name.

            **maxRetries** (str) - Max number of retries.

            **widget** (str) - Job Widget (JSON Form).

            **folder** (str) - Job Group.

            **icon** (str) - Font Awesome Icon.

        """

        res = self.connector.updateMany("Template", "removeJobs", sendNotifications, jobs)
        return None

    def createExternalInput(self, **kwargs):
        """List Projects

        Keyword Arguments:
            userOS (str): Client operating system.
            sokoObjectId (str): Object ID.
            sokoOutcomeTypeId (str): Output Type ID.
            authorId (str): Author ID.
            name (str): Name.
            comment (str): Comment.
            customProperties (str): Object Metadata.
            files (list): File paths.
        """

        res = self.connector.update("SokoObject", "createExternalInput", **kwargs)
        return res

    def sendJobWorkerPresence(self, is_online):
        """List JobInstances

        Args:
            is_online (bool): Worker is online.
        """

        _pc_name = platform.uname()[1]
        res = self.connector.update("Job", "sendJobWorkerPresence", **{'isOnline': is_online, 'workerName': _pc_name})
        return res

    def getProjects(self, **kwargs):
        """List Projects

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object identifier.
            statusId (str): Status ID.
            statusIds (str): List of Status IDs.
            clientId (str): Client ID.
            userId (str): Return projects where user is assignee.
            reviewerId (str): Return projects where user is reviewer.
            codeName (str): CodeName.
        """

        res = self.connector.get("Project", "getProjects", **kwargs)
        return SokoCollection(res["projects"], Project)

    def getProjectsFlat(self, statusIds=None):
        """List Projects flat (name, id)

        Args:
            statusIds (list): List of ProjectStatus IDs.
        """

        projs = self.getProjects()

        if statusIds:
            return sorted([(o.name, o.id) for o in projs if o.projectStatusId in statusIds], key=lambda x: x[0].lower())
        else:
            return sorted([(o.name, o.id) for o in projs], key=lambda x: x[0].lower())

    def getOutcomeTypes(self, **kwargs):
        """List SokoOutcomeTypes

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object identifier.
            name (str): Name.
            codeName (str): CodeName.
            screenPriority (int): ScreenPriority.
        """

        res = self.connector.get("template", "getSokoOutcomeTypes", **kwargs)
        return SokoCollection(res["sokoOutcomeTypes"], SokoOutcomeType)

    def getSokoObjectTypes(self, **kwargs):
        """List SokoObjectTypes

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object identifier.
            isTaskable (bool): Is Taskable.
            isCore (bool): Is Core.
            isTypeable (bool): Can have Type.
            isTodoable (bool): Can have Todos.
            name (bool): Name.
        """

        res = self.connector.get("template", "getSokoObjectTypes", **kwargs)
        return SokoCollection(res["sokoObjectTypes"], SokoObjectType)

    def getSokoObjectTemplates(self, **kwargs):
        """List SokoObjectTemplates

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object identifier.
            parentId (str): Parent ID.
            codeName (str): CodeName.
            returnRootOnly (bool): Return only root objects.
        """

        res = self.connector.get("Template", "getSokoObjectTemplates", **kwargs)
        return SokoCollection(res["sokoObjectTemplates"], SokoObjectTemplate)

    def getProjectDefinitions(self, **kwargs):
        """List ProjectDefinitions

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object identifier.
            ids (list): List of Object identifiers.
            isActive (bool): Is Active.
            hasBasicFolderStructure (bool): Return ProjectDefinitions with basic folder structure.
            returnDirectoryStructureSettings (bool): Return Directory Structure.
        """

        res = self.connector.get("Template", "getProjectDefinitions", **kwargs)
        return SokoCollection(res["projectDefinitions"], ProjectDefinition)

    def getFileTypes(self, **kwargs):
        """List FileTypes

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object identifier.
            fileExtension (str): Extension.
            isCore (bool): Object is Core.
        """

        res = self.connector.get("Template", "getFileTypes", **kwargs)
        return SokoCollection(res["fileTypes"], FileType)

    def getTypes(self, **kwargs):
        """List Types

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object identifier.
            name (str): Name.
        """

        res = self.connector.get("Template", "getTypes", **kwargs)
        return SokoCollection(res["types"], Type)

    def getCloudLinks(self, **kwargs):
        """List Cloud Links

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object identifier.
            ids (list): List of Object identifiers.
            webApplicationId (str): WebApplication ID.
            componentId (str): Component ID.
            componentIds (list): List of component IDs.
            originalId (str): 3rd party link ID.
        """

        res = self.connector.get("SokoObject", "getCloudLinks", **kwargs)
        return SokoCollection(res["cloudLinks"], CloudLink)

    def createCloudLink(self, **kwargs):
        """Creates Cloud Link

        Keyword Arguments:
            userOS (str): Client operating system.
            id (str): Object identifier.
            webApplicationId (str): WebApplication ID.
            componentId (str): Component ID.
            originalId (str): 3rd party link ID.
            link (str): Link.
        """

        res = self.connector.update("SokoObject", "createCloudLink", **kwargs)
        return res

    def updateCloudLink(self, **kwargs):
        """Updates Cloud Link

        Keyword Arguments:
            userOS (str): Client operating system.
            id (str): Object identifier.
            webApplicationId (str): WebApplication ID.
            componentId (str): Component ID.
            originalId (str): 3rd party link ID.
            link (str): Link.
        """

        res = self.connector.update("SokoObject", "updateCloudLink", **kwargs)
        return res

    def deleteCloudLink(self, **kwargs):
        """Deletes Cloud Link

        Keyword Arguments:
           id (str): Object Identifier.

        """

        res = self.connector.delete("SokoObject", "deleteCloudLink", **kwargs)
        return res

    def getComponents(self, **kwargs):
        """List Components

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object identifier.
            ids (list): List of Object identifiers.
            sokoOutcomeVersionId (str): Version ID.
            sokoOutcomeVersionIds (list): List of Version IDs.
            sokoOutcomeId (str): Output ID.
            sokoObjectId (str): SokoObject ID.
            sokoObjectIds (list): List of SokoObject IDs.
            fileTypeId (str): FileType ID.
            projectId (str): Project ID.
            path (str): File Path.
            paths (list): File Paths.
            isOnCloud (bool): Return File uploaded on Cloud.
            isOnLocal (bool): Return files on local drive.
        """

        res = self.connector.get("SokoObject", "getComponents", **kwargs)
        return SokoCollection(res["components"], Component)

    def createComponents(self, changes, sendNotifications=True):
        """Creates Components

        Args:
            changes (list of Change):  Changes.

        Change:
            **id** (str) - Component Identifier.

            **sokoOutcomeVersionId** (str) - Version ID.

            **temporaryCloudLinkId** (str) - Temporary CloudLink ID.

            **componentStateId** (str) - ComponentState ID.

            **fileTypeId** (str) - FileType ID.

            **relativePath** (str) - File Path.

            **isOnCloud** (bool) - Component is uploaded on cloud.

            **isOnLocal** (bool) - Component is on local drive.

            **modificationDate** (str) - Modification Date.

        """

        res = self.connector.updateMany("SokoObject", "createComponents", sendNotifications, changes)
        return res

    def updateComponent(self, **kwargs):
        """Updates Component

        Keyword Arguments:
            id (str): Component Identifier.
            sokoOutcomeVersionId (str): Version ID.
            temporaryCloudLinkId (str): Temporary CloudLink ID.
            cloudPlaces (list of CloudPlaces): CloudPlaces.
            fileTypeId (str): FileType ID.
            relativePath (str): File Path.
            isOnLocal (bool): Component is on local drive.
            modificationDate (str): Modification Date.

        CloudPlace:
             **id** (str) - Object Identifier.

             **cloudApplicationId** (str) - CloudApplication ID.

             **componentStateId** (str) - ComponentState ID.

             **componentId** (str) - Component ID.

        """

        res = self.connector.update("SokoObject", "updateComponent", **kwargs)
        return res

    def updateComponentBulk(self, changes, sendNotifications=True):
        """Updates SokoObjects

        Args:
            changes (list of Change):  Changes.
            sendNotifications (bool): Send server notification about change

        Change:
           **id** (str) - Object Identifier.

           **sokoOutcomeVersionId** (str) - Version ID.

           **temporaryCloudLinkId** (str) - Temporary CloudLink ID.

           **fileTypeId** (str) -  FileType ID.

           **relativePath** (str) - File Path.

           **isOnLocal** (bool) - Component is on local drive.

           **modificationDate** (str) - Modification Date.

           **cloudPlaces** (list of CloudPlaces) - CloudPlaces.

        CloudPlace:
             **id** (str) - Object Identifier.

             **cloudApplicationId** (str) - CloudApplication ID.

             **componentStateId** (str) - ComponentState ID.

             **componentId** (str) - Component ID.
        """

        res = self.connector.updateMany("SokoObject", "updateComponentBulk", sendNotifications, changes)
        return res

    def deleteComponent(self, **kwargs):
        """Deletes Component

        Keyword Arguments:
           id (str): Object Identifier.

        """

        res = self.connector.delete("SokoObject", "deleteComponent", **kwargs)
        return res

    def publish(self, **kwargs):
        """Publish new Version

        Keyword Arguments:
            userOS (str): Client operating system.
            sokoObjectId (str): Object ID.
            sokoOutcomeId (str): Output ID.
            sokoOutcomeTypeId (str): Output Type ID.
            authorId (str): Author ID.
            statusId (str): Status ID.
            suffix (str): Additional Output name.
            comment (str): Comment.
            customProperties (str): CustomProperties.
            components (list of Component): Components.

        Component:
            **id** (str) - Component Identifier.

            **sokoOutcomeVersionId** (str) - Version ID.

            **temporaryCloudLinkId** (str) - Temporary CloudLink ID.

            **componentStateId** (str) - ComponentState ID.

            **fileTypeId** (str) - FileType ID.

            **relativePath** (str) - File Path.

            **isOnCloud** (bool) - Component is uploaded on cloud.

            **isOnLocal** (bool) - Component is on local drive.

            **modificationDate** (str) - Modification Date.

        """

        outs = self.connector.publish(**kwargs)

        res = dict(
            components=SokoCollection(outs.get("components"), Component),
            sokoOutcomeVersion=SokoCollection(outs.get("versions"), SokoOutcomeVersion).first(),
            sokoOutcome=SokoCollection(outs.get("outcomes"), SokoOutcome).first(),
            componentIds=outs.get("componentIds"),
            sokoOutcomeVersionId=outs.get("versionId"),
            sokoOutcomeId=outs.get("outcomeId")
        )

        return res

    def publishExternalInput(self, **kwargs):
        """Publish new external input

        Keyword Arguments:
            userOS (str): Client operating system.
            targetSokoObjectId (str): Target Object ID.
            hiddenSokoObjectId (str): Hidden Object ID.
            sokoOutcomeId (str): Output ID.
            sokoOutcomeTypeId (str): Output Type ID.
            authorId (str): Author ID.
            statusId (str): Status ID.
            suffix (str): Additional Output name.
            comment (str): Comment.
            customProperties (str): CustomProperties.
            components (list of Component): Components.

        Component:
            **id** (str) - Component Identifier.

            **sokoOutcomeVersionId** (str) - Version ID.

            **temporaryCloudLinkId** (str) - Temporary CloudLink ID.

            **componentStateId** (str) - ComponentState ID.

            **fileTypeId** (str) - FileType ID.

            **relativePath** (str) - File Path.

            **isOnCloud** (bool) - Component is uploaded on cloud.

            **isOnLocal** (bool) - Component is on local drive.

            **modificationDate** (str) - Modification Date.

        """

        outs = self.connector.publish_external_input(**kwargs)

        res = dict(
            components=SokoCollection(outs.get("components"), Component),
            sokoOutcomeVersion=SokoCollection(outs.get("versions"), SokoOutcomeVersion).first(),
            sokoOutcome=SokoCollection(outs.get("outcomes"), SokoOutcome).first()
        )

        return res

    def getNoteCategories(self, **kwargs):
        """List NoteCategories

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object identifier.
            projectDefinitionId (str): ProjectDefinition ID.
        """

        res = self.connector.get("Template", "getNoteCategories", **kwargs)
        return SokoCollection(res["noteCategories"], NoteCategory)

    def getStatuses(self, stateIds=None, **kwargs):
        """List Priorities

        Args:
            stateIds (list): List of State Ids.

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object identifier.
            stateId (str): Name.
            frameIOLabelId (str): frame.io label ID.
            frameIOLabelIds (list): List of frame.io label IDs.
            name (str): Name.
            color (str): Color (HEX).
            canBeUsedForSokoObject (bool): Status is for Object.
            canBeUsedForSokoOutcome (bool): Status is for Version.
        """

        res = self.connector.get("Template", "getStatuses", **kwargs)
        sts = SokoCollection(res["statuses"], Status)
        if stateIds:
            sts = [s for s in sts if s.stateId in stateIds]
        return sts

    def getPriorities(self, **kwargs):
        """List Priorities

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object identifier.
            name (str): Name.
            value (int): Priority number.
        """

        res = self.connector.get("Template", "getPriorities", **kwargs)
        return SokoCollection(res["priorities"], Priority)

    def getAssigneeRoles(self, **kwargs):
        """List AssigneeRoles

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object identifier.
            name (str): Name.
            codeName (str): CodeName.
        """

        res = self.connector.get("UserManagement", "getAssigneeRoles", **kwargs)
        return SokoCollection(res["assigneeRoles"], AssigneeRole)

    def getFrameIOLabels(self, **kwargs):
        """List FrameIOLabels

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object identifier.
            name (str): Name.
            codeName (str): CodeName.
        """

        res = self.connector.get("Template", "getFrameIOLabels", **kwargs)
        return SokoCollection(res["frameIOLabels"], FrameIOLabel)

    def createJobInstance(self, **kwargs):
        """Creates JobInstance

        Keyword Arguments:
            userOS (str): Client operating system.
            id (str): Object identifier.
            createdAt (str): Create date.
            finishedAt (str): Finish date.
            status (str): Status.
            result (str): Job result.
            messageData (str): Job data.
            traceback (str): Error.
            contextId (str): Context ID.
            contextType (str): ContextType (project, soko_object, version).
        """

        res = self.connector.update("Job", "createJobInstance", **kwargs)
        return res

    def updateJobInstance(self, **kwargs):
        """Updates JobInstance

        Keyword Arguments:
            id (str): Object identifier.
            finishedAt (str): Finish date.
            status (str): Status.
            result (str): Job result.
            messageData (str): Job data.
            traceback (str): Error.
            contextId (str): Context ID.
            contextType (str): ContextType (project, soko_object, version).
        """

        res = self.connector.update("Job", "updateJobInstance", **kwargs)
        return res

    def getSokoObjects(self, **kwargs):

        """List SokoObjects

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object identifier.
            ids (list): List of Object identifiers.
            stateIds (list): List of States IDs.
            projectId (str): Project ID.
            sokoObjectTypeId (str): Object Type ID.
            sokoObjectTypeIds (list): List of Object Type IDs.
            excludeSokoObjectTypeIds (list): Exclude Object Types.
            statusId (str): Status ID.
            priorityId (str): Priority ID.
            typeId (str): Type ID.
            userId (str): Return Objects where User is an assignee.
            reviewerId (str): Return Objects where User is an reviewer. Return also Objects from projects where user is an reviewer.
            parentId (str): Parent ID.
            parentIds: (list) List of Parent IDs.
            names (list): List of Object names.
            name (str): Name.
            codeName (str): CodeName.
            nameFilter (str): Search string occurency in whole path.
            nameFilters (list): Search string occurency in whole path.
            returnPath (bool): Return Object path.
            returnHiddenObjects (bool): Return Hidden objects.
            returnCustomProperties (bool): Return Object metadata.
            isTaskable (bool): Return Taskable Objects.
            returnChildrenRecursive (bool): Return all children of Object.
            returnChildIds (bool): Return child object IDs.
            returnDirectChildIds (bool): Return direct child IDs.
            returnChildStatusIds (bool): Return child objects status IDs.
            returnChildStatusIdsByType (bool): Return child objects statuses grouped by type.
            returnDirectNonTaskableChildIds (bool): Return direct child non taskable IDs.
            returnParentIds (bool): Return parent IDs.
            returnIsAllChildTaskable (bool): Return if all children are taskable.
            returnHasTasks (bool): Return HasTasks property.
            returnMaxLevel (bool): Max tree level.
            onlyFromActiveProjects (bool): Return obly Objects from active projects.
            returnProjectObjects (bool): Return Root objects.
        """

        res = self.connector.get("SokoObject", "getSokoObjects", **kwargs)
        return SokoCollection(res["sokoObjects"], SokoObject)

    def getSokoObjectsByToken(self):
        """List SokoObjects for current User
        """

        res = self.connector.get("SokoObject", "getSokoObjectsByToken")
        return SokoCollection(res["sokoObjects"], SokoObject)

    def createSokoObject(self, **kwargs):
        """Creates SokoObject

        Keyword Arguments:
           id (str): Object Identifier.
           projectId (str): Project ID.
           sokoObjectTypeId (str): Output type ID.
           priorityId (str): Priority ID.
           statusId (str): Status ID.
           typeId (str): Type ID.
           parentId (str): Parent ID.
           name (str): Name.
           description (str): Description.
           startDate (str): StartDate.
           endDate (str): EndDate.
           deadLine (str): DeadLine.
           plannedHours (str): PlannedHours.
           managerIds (str): Manager IDs.
           reviewerIds (str): Reviewer IDs.
           assigneeIds (str): Assignee IDs.
           customProperties (str): Object Metadata.

        """

        res = self.connector.update("SokoObject", "createSokoObject", **kwargs)
        return res

    def createSokoObjectBulk(self, changes, sendNotifications=True):
        """Creates SokoObjects

        Args:
            changes (list of Change):  Changes.
            sendNotifications (bool): Send server notification about change

        Change:
           **id** (str) - Object Identifier.

           **projectId** (str) - SokoObject ID.

           **sokoObjectTypeId** (str) - Output type ID.

           **priorityId** (str) - Priority ID.

           **statusId** (str) - Status ID.

           **typeId** (str) - Type ID.

           **parentId** (str) - Parent ID.

           **name** (str) - Name.

           **description** (str) - Description.

           **startDate** (str) - StartDate.

           **endDate** (str) - EndDate.

           **deadLine** (str) - DeadLine.

           **plannedHours** (str) - PlannedHours.

           **managerIds** (str) - Manager IDs.

           **reviewerIds** (str) - Reviewer IDs.

           **assigneeIds** (str) - Assignee IDs.

           **customProperties** (str) - Object Metadata.

        """

        res = self.connector.updateMany("SokoObject", "createSokoObjectBulk", sendNotifications, changes)
        return res

    def createSokoObjectFromTemplate(self, **kwargs):
        """Updates SokoObject

        Keyword Arguments:
            userOS (str): Client operating system.
            targetIds (list): List of Target object IDs.
            sokoObjectTemplateId (str): Object Template ID.
            publisherId (str): Publisher ID.
            createOnlyIds (str): List of Objects to create.
            createSokoOutcomeTemplates (bool): Create saved Outputs.

        """

        res = self.connector.update("SokoObject", "createSokoObjectFromTemplate", **kwargs)
        return res

    def updateSokoObject(self, **kwargs):
        """Updates SokoObject

        Keyword Arguments:
           id (str): Object Identifier.
           projectId (str): SokoObject ID.
           sokoObjectTypeId (str): Output type ID.
           priorityId (str): Priority ID.
           statusId (str): Status ID.
           typeId (str): Type ID.
           parentId (str): Parent ID.
           name (str): Name.
           description (str): Description.
           startDate (str): StartDate.
           endDate (str): EndDate.
           deadLine (str): DeadLine.
           plannedHours (str): PlannedHours.
           managerIds (str): Manager IDs.
           reviewerIds (str): Reviewer IDs.
           assigneeIds (str): Assignee IDs.
           customProperties (str): Object Metadata.

        """

        res = self.connector.update("SokoObject", "updateSokoObject", **kwargs)
        return res

    def updateSokoObjectBulk(self, changes, sendNotifications=True):
        """Updates SokoObjects

        Args:
            changes (list of Change):  Changes.
            sendNotifications (bool): Send server notification about change

        Change:
           **id** (str) - Object Identifier.

           **projectId** (str) - SokoObject ID.

           **sokoObjectTypeId** (str) - Output type ID.

           **priorityId** (str) - Priority ID.

           **statusId** (str) - Status ID.

           **typeId** (str) - Type ID.

           **parentId** (str) - Parent ID.

           **name** (str) - Name.

           **description** (str) - Description.

           **startDate** (str) - StartDate.

           **endDate** (str) - EndDate.

           **deadLine** (str) - DeadLine.

           **plannedHours** (str) - PlannedHours.

           **managerIds** (str) - Manager IDs.

           **reviewerIds** (str) - Reviewer IDs.

           **assigneeIds** (str) - Assignee IDs.

           **customProperties** (str) - Object Metadata.

        """

        res = self.connector.updateMany("SokoObject", "updateSokoObjectBulk", sendNotifications, changes)
        return res

    def deleteSokoObject(self, **kwargs):

        """Deletes Object

        Keyword Arguments:
           id (str): Object Identifier.

        """

        res = self.connector.delete("SokoObject", "deleteSokoObject", **kwargs)
        return res

    def publishEvent(self, **kwargs):
        """Publish Event

        Keyword Arguments:
           data (str): Event Data.
           topic (str): Topic.

        """

        res = self.connector.input("Event", "publishEvent", **kwargs)
        return res

    def createSokoOutcome(self, **kwargs):
        """Creates Output

        Keyword Arguments:
           id (str): Object Identifier.
           sokoObjectId (str): SokoObject ID.
           sokoOutcomeTypeId (str): Output type ID.
           name (str): Output name.
           suffix (str): Additional Output name.

        """

        res = self.connector.update("SokoObject", "createSokoOutcome", **kwargs)
        return res

    def updateSokoOutcome(self, **kwargs):
        """Updates Output

        Keyword Arguments:
            id (str): Object Identifier.
            sokoObjectId (str): SokoObject ID.
            sokoOutcomeTypeId (str): Output type ID.
            name (str): Output name.
            suffix (str): Additional Output name.

        """

        res = self.connector.update("SokoObject", "updateSokoOutcome", **kwargs)
        return res

    def deleteSokoOutcome(self, **kwargs):
        """Deletes Output

        Keyword Arguments:
           id (str): Object Identifier.

        """

        res = self.connector.delete("SokoObject", "deleteSokoOutcome", **kwargs)
        return res

    def getSokoOutcomeVersions(self, **kwargs):
        """Creates Version

        Keyword Arguments:
           returnNested (bool): Return nested properties.
           returnRelations (bool): Return related properties.
           returnRelatedToUserIds (bool): Return list of User IDs related to object.
           userOS (str): Client operating system.
           id (str): Object identifier.
           ids (list): List of Object identifiers.
           sokoOutcomeId (str): Output ID.
           projectId (str): Project ID.
           sokoObjectIds (list): List of SokoObject IDs.
           sokoOutcomeIds (list): List of Output IDs.
           sokoObjectId (str): Object ID.
           authorId (str): Author ID.
           statusId (str): Status ID.
           stateId (str): State ID.
           reviewerId (str): Reviewer ID.
           versionNumber (int): Version Number.
           returnSokoObjectPath (bool): Return Object path.
           returnComponents (bool): Return version File Components.
           onlyFromActiveProjects (bool): Return versions from Active Projects.
           returnOnlyLatestVersion (bool): Return latest Versions.
           returnCustomProperties (bool): Return Version metadata.
           returnFromHiddenObjects (bool): Return External Input Versions.
           isMaster (bool): Is Master.
           statusIds (str): List of Status IDs.
           stateIds (str): List of State IDs.

        """

        res = self.connector.get("SokoObject", "getSokoOutcomeVersions", **kwargs)
        return SokoCollection(res["sokoOutcomeVersions"], SokoOutcomeVersion)

    def createSokoOutcomeVersion(self, **kwargs):
        """Creates Version

        Keyword Arguments:
           id (str): Object Identifier.
           sokoOutcomeId (str): Output ID.
           authorId (str): Author ID.
           statusId (str): Status ID.
           isMaster (str): IsMaster.
           isClientVersion (str): IsClientVersion.
           customProperties (str): CustomProperties.

        """

        res = self.connector.update("SokoObject", "createSokoOutcomeVersion", **kwargs)
        return res

    def updateSokoOutcomeVersion(self, **kwargs):
        """Updates Version

        Keyword Arguments:
           id (str): Object Identifier.
           sokoOutcomeId (str): Output ID.
           authorId (str): Author ID.
           statusId (str): Status ID.
           isMaster (str): IsMaster.
           isClientVersion (str): IsClientVersion.
           customProperties (str): CustomProperties.

        """

        res = self.connector.update("SokoObject", "updateSokoOutcomeVersion", **kwargs)
        return res

    def deleteSokoOutcomeVersion(self, **kwargs):
        """Deletes Version

        Keyword Arguments:
           id (str): Object Identifier.

        """

        res = self.connector.delete("SokoObject", "deleteSokoOutcomeVersion", **kwargs)
        return res

    def addAssigneeToSokoObjectBatch(self, changes, sendNotifications=True):
        """Assign User to Object

        Args:
            changes (list of Change):  Changes.
            sendNotifications (bool): Send server notification about change

        Change:
           **id** (str) - Object Identifier.

           **userId** (str) - User ID.

           **assigneeRoleId** (str) - AssigneeRole ID.

           **sokoObjectId** (str) - Object ID.

        """

        res = self.connector.updateMany("SokoObject", "addAssigneeToSokoObjectBatch", sendNotifications, changes)
        return res

    def addAssigneeToSokoObject(self, **kwargs):
        """Assign User to Object

        Keyword Arguments:
           id (str): Object Identifier.
           userId (str): User ID.
           assigneeRoleId (str): AssigneeRole ID.
           sokoObjectId (str): Object ID.

        """

        res = self.connector.update("SokoObject", "addAssigneeToSokoObject", **kwargs)
        return res

    def addCustomProperty(self, entity, id, key, value):
        """Add metadata

        Args:
           entity (str): Entity type (SokoObject, SokoOutcomeVersion).
           id (str): Object Identifier.
           key (str): Key.
           value (str): Value.

        """

        if entity.lower() == "sokoobject":
            obj = self.getSokoObjects(id=id, returnCustomProperties=True).first()
            if not obj:
                raise Exception("Object not found.")

            if not obj:
                raise Exception("Object not found.")

            if obj.customProperties:
                _customProperties = json.loads(obj.customProperties)
                _customProperties[key] = value
            else:
                _customProperties = {}
                _customProperties[key] = value

            return self.updateSokoObject(id=id, customProperties=json.dumps(_customProperties))

        elif entity.lower() == "sokooutcomeversion":
            obj = self.getSokoOutcomeVersions(id=id, returnCustomProperties=True).first()
            if not obj:
                raise Exception("Object not found.")

            if obj.customProperties:
                _customProperties = json.loads(obj.customProperties)
                _customProperties[key] = value
            else:
                _customProperties = {}
                _customProperties[key] = value

            return self.updateSokoOutcomeVersion(id=id, customProperties=json.dumps(_customProperties))
        else:
            raise Exception("Not suported entity type.")

    def assigneeUpdateSokoObjectStatusBulk(self, changes, sendNotifications=True):
        """Update Objects Status

        Args:
            changes (list of Change):  Changes.
            sendNotifications (bool): Send server notification about change

        Change:
           **id** (str) - Object Identifier.

           **statusId** (str) - Status ID.

        """

        res = self.connector.updateMany("SokoObject", "assigneeUpdateSokoObjectStatusBulk", sendNotifications, changes)
        return res

    def getDataForShare(self, **kwargs):
        """Get data for share

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            sokoOutcomeVersionIds (list): List of Version IDs.

        """

        res = self.connector.get("Integration", "getDataForShare", **kwargs)
        return res["shareInfo"]

    def getDataForUnshare(self, **kwargs):
        """Get data for unshare

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            sokoObjectId (str): Object ID.
            userId (str): User ID.

        """

        res = self.connector.get("Integration", "getDataForUnshare", **kwargs)
        return res["shareInfo"]

    def getDataForShareForSokoObject(self, **kwargs):
        """Get data for share

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            sokoObjectId (str): Object ID.
            userId (str): User ID.

        """

        res = self.connector.get("Integration", "getDataForShareForSokoObject", **kwargs)
        return res["shareInfo"]

    def createNote(self, **kwargs):
        """Creates Note

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object Identifier.
            sokoObjectId (str): SokoObject ID.
            sokoOutcomeVersionId (str): Version ID.
            parentId (str): Parent ID.
            authorId (str): Author ID.
            noteCategoryId (str): Note Category ID.
            content (str): Note content.
            isPinned (bool): Is Note pinned (on top of Note list)
            isAutogenerated (bool): Is Note autogenerated.
            attachments(list of Attachment): List of Attachments.

        Attachment:
            **userOS** (str) - Client operating system.

            **id** (str) - Object Identifier.

            **fileName** (str) - File name.

            **contextType** (str) - Context Type (note, soko_object).

            **attachmentId** (str) - Attachment ID.

            **contextId** (str) - Context ID.

            **authorId** (str) - Author ID.

            **mimeType** (str) - File mime Type.

        """

        res = self.connector.update("SokoObject", "createNote", **kwargs)
        return res

    def createNotification(self, **kwargs):
        """Creates Notification

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object Identifier.
            userId (str): User ID.
            seen (str): If is notification seen.
            authorId (str): Author ID.
            contextId (str): Context ID.
            objectId (str): Object ID.
            contextType (str): Context Type (soko_object).
            objectType (str): Object Type (job, note, soko_object, soko_outcome_version).
            changes (str): JSON string with changes.
            label (str): Label.
            action (str): Action.
            notificationTypeId (str): Notification Type ID.

        """

        res = self.connector.update("UserManagement", "createNotification", **kwargs)
        return res

    def createUpdateUploadRecord(self, **kwargs):
        """Create or Update UploadRecord

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object Identifier.
            jobId (str): Job ID.
            status (str): Status.
            fileName (str): File Name.
            path (str): File Path.
            traceback (str): Error.
            startedAt (str): Start date.
            authorId (str): Author ID.
            total (int): Total progress.
            progress (int): Actual progress.
            retries (int): Number of retries.

        """

        res = self.connector.update("Job", "createUpdateUploadRecord", **kwargs)
        return res

    def publishJob(self, **kwargs):
        """Get External Inputs

        Keyword Arguments:
            userOS (str): Client operating system.
            id (str): Object Identifier.
            jobName (str): JobName.
            status (str): Job status (failed, success, running).
            data (str): Job data.
            contextId (str): Context ID.
            contextType (str): Context Type (project, soko_object, version).

        """

        res = self.connector.update("Job", "publishJob", **kwargs)
        return res

    def uploadThumbnail(self, file_path, identifier, entity_type, mime_type):
        if not identifier:
            raise Exception("Field Identifier is required.")

        upload_res = self.connector._upload2("Thumbnail", file_path, identifier, entity_type, mime_type)
        return upload_res

    def uploadAttachment(self, file_path, identifier):
        if not identifier:
            raise Exception("Field Identifier is required.")

        upload_res = self.connector.upload(file_path, identifier)
        return upload_res

    def createAttachment(self, file_path, id=None, contextType=None, contextId=None, attachmentId=None, authorId=None):
        if not attachmentId:
            raise Exception("Field AttachmentId is required.")

        upload_res = self.connector.upload(file_path, attachmentId)

        ch = {
            "id": id,
            "fileName": os.path.basename(file_path),
            "contextType": contextType,
            "contextId": contextId,
            "attachmentId": attachmentId,
            "authorId": authorId if authorId else self.user().id,
            "mimeType": upload_res['mimetype']
        }

        res = self.connector.update("SokoObject", "createAttachment", **ch)
        return res

    def getExternalInputs(self, **kwargs):
        """Get External Inputs

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            sokoObjectId (str): Object ID.
            sokoObjectIds (list): List of Object IDs.

        """

        res = self.connector.get("SokoObject", "getExternalInputs", **kwargs)
        return SokoCollection(res["sokoOutcomes"], SokoOutcome)

    def getSokoOutcomes(self, **kwargs):
        """Get Outputs

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object Identifier.
            ids (list): List of Object identifiers.
            sokoObjectId (str): Object ID.
            sokoObjectIds (list): List of Object IDs.
            projectId (str): Project ID.
            projectIds (list): List of Project IDs.
            userId (str): User email.
            sokoOutcomeTypeId (str): User is active.
            sokoOutcomeTypeIds (list): List of Output Type IDs.
            nameFilters (list): List of strings for filter.
            name (str): Name.
            suffix (str): Additional Output name.
            fullName (str): Name with Suffix.
            returnVersions (bool): Return Versions.
            returnComponents (bool): Return File Components.
            returnOnlyLatestVersion (bool): Return Latest Versions.
            returnFromHiddenObjects (bool): Return External Inputs.
            onlyFromActiveProjects (bool): Return Outputs from active projects.
            hiddenInOutgoing (bool): Return Outputs hidden in outgoing objects.

        """

        res = self.connector.get("SokoObject", "getSokoOutcomes", **kwargs)
        return SokoCollection(res["sokoOutcomes"], SokoOutcome)

    def getUsers(self, **kwargs):
        """Get Users

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object Identifier.
            ids (list): List of Object identifiers.
            securityRoleId (str): SecurityRole ID.
            userName (str): UserName.
            email (str): User email.
            isActive (bool): User is active.
            isExtern (bool): User is external.
            returnClientSecurityAllowances (bool): Return user allowances.
            returnNotifications (bool): Return user notifications.
            returnAppLastState (bool): Return user app state.

        """

        res = self.connector.get("UserManagement", "getUsers", **kwargs)
        return SokoCollection(res["users"], User)

    def getSokoObjectAssignees(self, **kwargs):
        """Get Object Assignees

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object Identifier.
            ids (list): List of Object identifiers.
            userId (str): User ID.
            assigneeRoleId (str): AssigneeRole ID.
            assigneeRoleIds (str): List of AssigneeRole IDs.
            sokoObjectId (str): Object ID.
            sokoObjectIds (list): List of SokoObject IDs.
            isExternal (bool): Is External.
            projectId (str): Project ID.

        """

        res = self.connector.get("SokoObject", "getSokoObjectAssignees", **kwargs)
        return SokoCollection(res["assignees"], Assignee)

    def getSokoObjectPath(self, **kwargs):
        """Get Object Path

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object Identifier.

        """

        res = self.connector.get("SokoObject", "getSokoObjectPath", **kwargs)
        return SokoCollection(res["objectPath"], DynamicSokoObject)

    def getObjectPath(self, obj):
        """Get Object Path

        Args:
            obj (SokoObject): Return nested properties.
        """

        objs = []
        tmp = obj
        while not tmp["parent"] == None:
            objs.append(tmp)
            tmp = tmp["parent"]

        return reversed(objs)

    def getSokoOutcomeConfigurations(self, **kwargs):
        """Get Output configurations

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object Identifier.
            name (str): Name.
            applicationId (str): Application ID.
            sokoOutcomeTypeId (str): Output Type ID.
            isImportConfiguration (bool): Is Configuration for Import.
            isPublishConfiguration (bool): Is Configuration for Publish.
        """

        res = self.connector.get("Template", "getSokoOutcomeConfigurations", **kwargs)
        return SokoCollection(res["sokoOutcomeConfigurations"], SokoOutcomeConfiguration)

    def getApplications(self, **kwargs):
        """Get Output name

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            id (str): Object Identifier.
            typeId (str): Type ID.
            sokoObjectId (str): Object ID.
            isActive (bool): Is Active.
            isSoftware (bool): Is Software.
        """

        res = self.connector.get("Template", "getApplications", **kwargs)
        return SokoCollection(res["applications"], Application)

    def getSokoOutcomeDefaultName(self, **kwargs):
        """Get Output name

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            sokoObjectId (str): SokoObject ID.
            sokoOutcomeTypeId (str): Output Type ID.
            sokoOutcomeSuffix (str): Additional Output name.
            publisherId (str): Author ID.
        """

        res = self.connector.get("SokoObject", "getSokoOutcomeDefaultName", **kwargs)
        return res["response"]

    def getSokoOutcomeVersionFileName(self, **kwargs):
        """Get File Name for Version

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            sokoObjectId (str): SokoObject ID.
            sokoOutcomeTypeId (str): Output Type ID.
            sokoOutcomeName (str): Output Name.
            sokoOutcomeSuffix (str): Additional Output name.
            publisherId (str): Author ID.
            version (int): Version number.
        """

        res = self.connector.get("SokoObject", "getSokoOutcomeVersionFileName", **kwargs)
        return res["response"]

    def getSokoOutcomePublishPath(self, **kwargs):
        """Get Publish path

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            sokoObjectId (str): SokoObject ID.
            sokoOutcomeTypeId (str): Output Type ID.
            sokoOutcomeName (str): Output Name.
            sokoOutcomeSuffix (str): Additional Output name.
            publisherId (str): Author ID.
            version (int): Version number.
        """

        res = self.connector.get("SokoObject", "getSokoOutcomePublishPath", **kwargs)
        return res["response"]

    def getEventsUrl(self, **kwargs):
        """Get Server Events URL
        """

        res = self.connector.get("Event", "getEventsUrl", **kwargs)
        return res["response"]

    def get3DRenderInfo(self, **kwargs):
        """Get 3D Render Info

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            sokoObjectId (str): SokoObject ID.
            sokoOutcomeTypeId (str): OutputType ID.
            sokoOutcomeSuffix (str): Additional Output name.

        """

        res = self.connector.get("Integration", "get3DRenderInfo", **kwargs)
        return res["renderInfo"]

    def getInitialDataIntegration(self, **kwargs):
        """Get initial integration data

        Keyword Arguments:
            returnNested (bool): Return nested properties.
            returnRelations (bool): Return related properties.
            returnRelatedToUserIds (bool): Return list of User IDs related to object.
            userOS (str): Client operating system.
            sokoOutcomeStateIds (list): List of Output State ID.
            sokoObjectStateIds (list): List of Object State ID.
            projectStatusIds (list): List of ProjectStatus IDs.
            applicationId (str): Application ID.

        """

        res = self.connector.get("Integration", "getInitialData", **kwargs)

        data = dict(
            projects=sorted([(o.name, o.id) for o in SokoCollection(res.get("projects", []), Project)],
                            key=lambda x: x[0].lower()),
            users=SokoCollection(res.get('users', []), User),
            sokoOutcomeStatuses=sorted(SokoCollection(res.get("sokoOutcomeStatuses", []), Status),
                                       key=lambda x: x.state.statusScreenPriority, reverse=True),
            sokoObjectStatuses=sorted(SokoCollection(res.get("sokoObjectStatuses", []), Status),
                                      key=lambda x: x.state.statusScreenPriority, reverse=True),
            publishConfigurations=SokoCollection(res.get('publishConfigurations', []),
                                                 SokoOutcomeConfiguration),
            importConfigurations=SokoCollection(res.get('importConfigurations', []), SokoOutcomeConfiguration),
            application=Application(res.get('application', None))
        )

        return data

    def getSokoObjectsTree(self, projectId):
        """Get Objects as Tree

        Args:
            projectId (str): Project ID.

        """

        objs = self.getSokoObjects(projectId=projectId)

        if objs == None:
            return []

        if len(objs) == 0:
            return []

        s_objs = []
        for i in objs:
            if not isinstance(i, SokoObject):
                continue
            else:
                s_objs.append(i)

        classes = []
        for item in s_objs:
            name = item['id']
            if name not in classes:
                classes.append(name)

        treenodes = {}
        root_node = None

        for item in s_objs:  # Create  tree nodes
            item['children'] = []
            name = item['id']
            treenodes[name] = item
            parent = item['parentId']
            if parent not in classes:  # parent is root node, create
                if parent not in treenodes:
                    node = {}
                    node['parentId'] = None
                    node['parent'] = None
                    node['children'] = []
                    node['id'] = parent
                    root_node = node
                    treenodes[parent] = node

        # Connect parents and children
        for item in s_objs:  # Create  tree nodes
            parent = item['parentId']
            parent_node = treenodes[parent]
            parent_node['children'].append(item)
            item["parent"] = parent_node

        return root_node["children"]
