Publishing
==========

Publishing new version is one of an essential feature of Soko. This tutorial will show you how to create new version.

For publish new **Version**, first you need to create an **Output** on Task. Each **Outputs** must have **Type**. To get type, you can call ``getOutcomeTypes`` method::

   image_type = api.getOutcomeTypes(name="Image")

Create Output
*************

To create new Output, use ``createSokoOutcome`` method::

   outcome = api.createSokoOutcome(
       id="b02aea03-548e-43aa-afc0-acf54e88fade",
       sokoObjectId=task_id,
       sokoOutcomeTypeId=image_type[0].id,
       name="my image"
   )

By default, Soko will generate **Output name** by your **Project Definition settings**. With publishing via API, you can pass your custom Output name or
grab one from API::

   outcome_name = api.getSokoOutcomeDefaultName(
       sokoObjectId=task_id,
       sokoOutcomeTypeId=image_type[0].id,
       sokoOutcomeSuffix="additional name"
   )

   outcome_id = outcome["response"]

.. note::
    Output can have an **Suffix**. Suffix is and additional name to specify an Output.

.. note::
    There are also ``getSokoOutcomes``, ``updateSokoOutcome`` and ``deleteSokoOutcome`` methods for managing **Outputs**


Create new Version
******************

Versions like SokoObjects have status. There are two types of status:

* statuses for Versions
* statuses for Objects

List only statuses for versions::

   outcome_statuses = api.getStatuses(canBeUsedForSokoOutcome=True)

To create new Version, use ``createSokoOutcomeVersion`` method::

   version = api.createSokoOutcomeVersion(
           id="70ec7e06-f872-42c4-bf87-d241cb9a8e17",
           sokoOutcomeId=outcome_id,
           authorId=users[0].id,
           statusId=outcome_statuses[0].id,
           comment="new version!"
   )

   version_id = version["response"]

.. note::
    There are also ``getSokoOutcomeVersions``, ``updateSokoOutcomeVersion`` and ``deleteSokoOutcomeVersion`` for managing **Versions**

Add files to version
********************

Versions have to contain files. To attach files into Version, call ``createComponents`` with list of files::

   components = api.createComponents([
       {
           "id": "2e870ead-6929-4add-903e-524112867897",
           "sokoOutcomeVersionId": version_id,
           "relativePath": "path/to/file.jpeg"
       }
   ])

.. note::
    There are also ``getComponents``, ``updateComponent`` and ``deleteComponent`` for managing **Components**

Getting correctly generated file paths and names
************************************************

The advantage of Soko is **naming convention** of published files. To get file name generated from **Project Definition** settings, call::

   file_name = api.getSokoOutcomeVersionFileName(
       sokoObjectId=task_id,
       sokoOutcomeTypeId=image_type[0].id,
       sokoOutcomeSuffix="additional name",
       sokoOutcomeName=outcome_name,
       publisherId=users[0].id,
       version=1
   )

To get file path generated from Project Definition settings, call::

   output_paths = api.getSokoOutcomePublishPath(
       sokoObjectId=task_id,
       sokoOutcomeTypeId=image_type[0].id,
       sokoOutcomeName=outcome_name,
       sokoOutcomeSuffix="additional name",
       publisherId=users[0].id
   )

You can get all needed data in one call. If you specify ``sokoOutcomeId``, next version number is automatically used
or you can specify yout custom version number with ``versionNumber`` parameter::

   publish_info = api.getPublishInfo(
       sokoObjectId=task_id,
       sokoOutcomeTypeId=image_type[0].id,
       sokoOutcomeName=outcome_name,
       sokoOutcomeId=outcome_id,
       suffix="additional name",
       authorId=users[0].id,
       versionNumber=5 # optional
   )

Publishing new version
**********************

For simplification publish process, you can use ``publish`` method::

   res = api.publish(
       sokoObjectId=task_id,
       sokoOutcomeId=outcome_id,
       sokoOutcomeTypeId=image_type[0].id,
       statusId=outcome_statuses[0].id,
       suffix="additional name",
       comment="new version!",
       components=[
           "path/to/file.jpeg"
       ],
       returnCreatedObjects=True
   )

You can also save some metadata by pass ``customProperties`` parameter as JSON string::

   metadata = {
       "key": "value"
   }

   res = api.publish(
       ...
       customProperties=json.dumps(metadata)
   )


Publish example
---------------

Complete working publish example::

   import os
   import shutil

   publish_info = api.getPublishInfo(
       sokoObjectId=task_id,
       sokoOutcomeTypeId=image_type[0].id,
       suffix="additional name",
       authorId=users[0].id
   )

   publish_paths = publish_info["publishPaths"]
   file_name = publish_info["fileName"]

   source = "path/to/file.jpeg"

   components = []
   for path in publish_paths:
       destination = '/'.join([path, file_name + os.path.splitext(source)[-1]])

       if not os.path.exists(path):
           os.makedirs(path)

       shutil.copy2(source, destination)
       components.append(destination)

   metadata = {
       "key": "value"
   }

   publish_result = api.publish(
       sokoObjectId=task_id,
       sokoOutcomeId=outcome_id,
       sokoOutcomeTypeId=image_type[0].id,
       statusId=outcome_statuses[0].id,
       suffix="additional name",
       comment="new version!",
       components=components,
       customProperties=json.dumps(metadata),
       returnCreatedObjects=True
   )