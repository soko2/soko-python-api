Log into API
=================

In this tutorial you will learn how to log in Soko API.

First, import Soko API::

   from soko_api import SokoApi

``SokoApi`` is a **Singleton** class. You can create instance and save it in to variable, or call constructor each time you access API. To login into Soko API with ``API Key`` and yourt ``Soko Server URL``::

   API_KEY = "YOUR_API_KEY"
   SERVER_URL = "https://my-soko.eu/api"

   api = SokoApi()
   api.set_server_url(SERVER_URL)
   api.login(apiKey=API_KEY)

Now you can use Soko Python API