Working with objects
====================

In this tutorial you will learn how to create, update and delete objects in Soko. This tutorial will also show you how to get data from Soko and assign user to task.

After log into API, you can list all projects::

   projects = api.getProjects()

To get project objects, you can query ``getSokoObjects`` method. In each methods, you can use filters.
Filters can be passed as a keyword arguments
In this example, we list all objects from certain project::

   objects = api.getSokoObjects(projectId=projects[0].id)

   [<soko_api.model.models.SokoObject object at 0x03C6F770>, <soko_api.model.models.SokoObject object at 0x03C6F830>, <soko_api.model.models.SokoObject object at 0x03C6F1D0>, <soko_api.model.models.SokoObject object at 0x03C6F2D0>, <soko_api.model.models.SokoObject object at 0x03C6F8D0>, <soko_api.model.models.SokoObject object at 0x03C6F7D0>]

Creating objects
****************

All projects have by default **root** objects. You can list root object of project like this::

   root = api.getSokoObjects(
       projectId=projects[0].id,
       returnProjectObjects=True
   )

.. note::

    To create object, you need to provide:

    ``id`` - UUID identifier of object

    ``name`` - Name of the object

    ``projectId`` - Id of project which own object

    ``sokoObjectTypeId`` - Type of an object. By default, all objects in Soko are generic, SokoObjectType identifies type of an object

    ``priorityId`` - Object priority for artist. Its usefull on 'Task' objects.

    ``statusId`` - Current status of object.

Create new Folder object, use root object ID as ``parentId`` of new Folder::

   object_statuses = api.getStatuses(canBeUsedForSokoObject=True)
   priorities = api.getPriorities()
   task_type = api.getSokoObjectTypes(name="Task")[0]
   folder_type = api.getSokoObjectTypes(name="Folder")[0]
   compositing_type = api.getTypes(name="Compositing")[0]

   folder = api.createSokoObject(
       id="a7e904dd-aa70-488e-97c3-6da70d0083f8",
       parentId=root[0].id,
       projectId=projects[0].id,
       sokoObjectTypeId=folder_type.id,
       priorityId=priorities[0].id,
       statusId=object_statuses[0].id,
       name="new folder"
   )

Create new Task object, use our Folder object ID as ``parentId`` of new Task. For **Taskable** objects, you can also specify ``typeId``::

   task = api.createSokoObject(
       id="594be487-ea2e-45b8-9a4c-91f54c963c9e",
       parentId=folder["response"],
       projectId=projects[0].id,
       sokoObjectTypeId=task_type.id,
       priorityId=priorities[0].id,
       statusId=object_statuses[0].id,
       name="new task",
       typeId=compositing_type.id # Compositing
   )

   task_id = task["response"]

Update object
*************
To update object, call ``updateSokoObject`` method with ID of object you want update and new property::

   updateResponse = api.updateSokoObject(
       id=task_id,
       statusId="3e788227-5759-440c-abe3-8a1abbe3f502" # Ready To Start
   )

Delete object
*************
To detele object, call ``deleteSokoObject`` method with ID of object you want to delete::

   deleteResponse = api.deleteSokoObject(
       id=task_id
   )

Assign User to Task
*******************

You can assign User to Task. For assign user, you need to provide ``assigneeRoleTypeId``.
In Soko are 3 types of Assignee Role:

.. note::
    **Assignee** - ``12b42675-3456-4708-bf07-884775f88cd7``

    **Reviewer** - ``dc44f144-e264-4583-b02c-5f2afbaf8a72``

    **Manager**- ``3a9e2653-4de8-4b4e-a210-2ffa48408648``

To list all Assignee Roles, call this method::

   assignee_roles = api.getAssigneeRoles()

List Users::

   users = api.getUsers()

Assign User as **assignee** to task::

   assignment = api.addAssigneeToSokoObject(
       id="8653f5aa-f857-438c-9ede-86f27da47d40",
       sokoObjectId=task_id,
       userId=users[0].id,
       assigneeRoleId="12b42675-3456-4708-bf07-884775f88cd7"
   )