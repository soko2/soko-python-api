.. Soko Python API documentation master file, created by
   sphinx-quickstart on Wed Jun 29 13:33:22 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Soko Python API's documentation!
===========================================

.. toctree::
   :maxdepth: 3

   tutorial/login
   tutorial/manage-objects
   tutorial/publishing
   tutorial/api/modules

